'use strict';

var u = require(__dirname + "/lib/utils.js"),
    log = u.log;

module.exports = function (app) {
    var ctrl = require('./controller.js');

    app.route('/newuser')
        .post(ctrl.newuser);

    app.route('/getusers')
        .get(ctrl.getusers);

    app.route('/getuser')
        .get(ctrl.getuser);

    app.route('/getrating')
        .get(ctrl.getrating);

    app.route('/getuserfriends')
        .get(ctrl.getfriends);

    app.route('/adduserfriends')
        .post(ctrl.addfriends);


    app.route('/getnearplaces')
        .get(ctrl.getnearplaces);

    app.route('/getplaces')
        .get(ctrl.getplaces);

    app.route('/getplace')
        .get(ctrl.getplace);


    app.route('/getachs')
        .get(ctrl.getachs);

    app.route('/getach')
        .get(ctrl.getach);

    /*
        app.route('/getdaily')
            .post(ctrl.getd);

        app.route('/submitdaily')
            .post(ctrl.submitd);
    */

    app.route('/getcomment')
        .get(ctrl.getc);

    app.route('/addcomment')
        .post(ctrl.addc);

    app.route('/like')
        .post(ctrl.like);

    app.route('/capture')
        .post(ctrl.capture);

    app.route('/newcapture')
        .post(ctrl.newcapture);

    app.route('/upload')
        .post(ctrl.upload);

    app.route('/getdata')
        .post(ctrl.dataFromPhoto);

    app.route('/newplace')
        .post(ctrl.newplace)
};