var fb = require(__dirname + "/firebase.js"),
    mg = require(__dirname + "/mongoose.js"),
    u = require(__dirname + "/utils.js"),
    db = require(__dirname + "/db.js"),
    log = console.log;

var updateAchForUser = function(uid) {
    return new Promise(function(resolve, reject) {});
}

var sendAchFR = function(uid) {
    return new Promise(function(resolve, reject) {
        db.appendUserAch(uid, 0).then(() => {
            resolve();     
            return fb.sendToUser(uid, "ach", { "type": 0, "cmd": 0, "uid": 0, "data": "" }); // type=0(ach) uid=0(reg)
        }).then(() => {
        }).catch((err) => {
            reject();
        });
    });
}

var sendAchFF = function(uid) {
    return new Promise(function(resolve, reject) {
        db.getUser(uid).then((res) => {
            resolve();
            if (res.friends.length != 0) return resolve(0);
            else return db.appendUserAch(uid, 1);
        }).then((res) => {
            if (res == 0){
                return fb.sendToUser(uid, "ach", { "type": 0, "cmd": 0, "uid": 1, "data": "" }); // type=0(ach) uid=0(fri)
            }
        }).then((res) => {
        }).catch((err) => {
            reject();
        });
    });
}


var sendAchFC = function(uid) {
    return new Promise(function(resolve, reject) {
        db.getCommentsForUser(uid).then((res) => {
            resolve();
            if (res.length == 0) return false;
            else return db.appendUserAch(uid, 2);
        }).then((res) => {
            if (res != false)
                return fb.sendToUser(uid, "ach", { "type": 0, "cmd": 0, "uid": 2, "data": "" }); // type=0(ach) uid=0(com)
        }).then((res) => {
        }).catch((err) => {
            reject();
        });
    });
}

var sendAchPC = function(uid, num) {
    return new Promise(function(resolve, reject) {
        db.getUser(uid).then((res) => {
            resolve();
            if(res.cap.length+1 != num) return false;
            else return db.appendUserAch(uid, 3+(num == 1 ? 0 : (num == 10 ? 1 : (num == 20 ? 2 : (num == 50 ? 3 : (num == 100 ? 4 : 5))))));
        }).then((res) => {
            if (res != false)
                return fb.sendToUser(uid, "ach", { "type": 0, "cmd": 0, "uid": 2, "data": "" }); // type=0(ach) uid=0(com)
        }).then((res) => {
        }).catch((err) => {
            reject();
        });
    });
}


var sendAchFPCreate = function(uid) {
    return new Promise(function(resolve, reject) {
        db.getUser(uid).then((res) => {
            resolve();
            if (res.ach.indexOf(11) == -1) return false;
            else return db.appendUserAch(uid, 11);
        }).then((res) => {
            if (res != false)
                return fb.sendToUser(uid, "ach", { "type": 0, "cmd": 0, "uid": 11, "data": "" }); // type=0(ach) uid=0(com)
        }).then((res) => {
        }).catch((err) => {
            reject();
        });
    });
}


module.exports.sendAchFR = sendAchFR;
module.exports.sendAchFF = sendAchFF;
module.exports.sendAchFC = sendAchFC;
module.exports.sendAchPC = sendAchPC;
module.exports.sendAchFPCreate = sendAchFPCreate;

/*
setTimeout(() => {
sendAchFR("cQHz8IzG1jhriVMIBrFArYwxGfW2")
}, 7000);
*/

/*
fr - when user first register       uid=0
ff - first friend                   uid=1
fc - first comment                  uid=2
c.<x> - when captured x places      uid=3,4,5,6,7,8 x=1,10,20,50,100,1000
author - 0
architect - 1
historican - 2
traveler - 3
*/