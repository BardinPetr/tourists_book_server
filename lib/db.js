var mongo = require(__dirname + "/mongoose.js"),
    u = require(__dirname + "/utils.js"),
    assert = require('assert'),
    log = console.log;

var updateUser = function (q, r) {
    return new Promise(function (resolve, reject) {
        mongo.userM.findOneAndUpdate(q, {
            $set: r
        }, {}, (err, data) => {
            if (err) return reject();
            else return resolve();
        })
    });
}

var newUser = function (data, rdata) {
    console.log("Creating new profile for user");
    return new Promise(function (resolve, reject) {
        var newUser = new mongo.userM({
            uid: data.uid,
            name: (data.name ? data.name : (rdata.name ? rdata.name : data.email)),
            email: data.email,
            cityid: 0, //That's line is a huge crutch
            photo: (data.picture ? data.picture : "https://www.shareicon.net/download/2017/01/06/868320_people_512x512.png"),
            friends: []
        });
        newUser.save((err) => {
            if (err) {
                log("Creating new profile: error: " + err);
                reject();
            } else {
                log("Creating new profile: ok");
                resolve();
            }
        })
    });
}


var isRegistred = function (uid) {
    return new Promise(function (resolve, reject) {
        mongo.userM.count({
            uid: uid
        }).exec((err, res) => {
            if (err) return reject();
            else return resolve(res > 0);
        });
    });
}

var getUser = function (uid) {
    return new Promise(function (resolve, reject) {
        mongo.userM.findOne({
            uid: uid
        }).exec((err, user) => {
            if (err) return reject();
            else return resolve(user);
        });
    });
}

var getUsers = function (a) {
    if (!a) a = {};
    return new Promise(function (resolve, reject) {
        mongo.userM.find(a).exec((err, user) => {
            if (err) return reject();
            else return resolve(user);
        });
    });
}


var newPlace = function (data) {
    log("Creating new place: " + data.uid);
    return new Promise(function (resolve, reject) {
        var newPlace = new mongo.placeM({
            uid: data.uid,
            name: data.name,
            desc: data.desc,
            url: data.url,
            rew: data.rew,
            totcap: 0,
            type: data.type,
            loc: data.loc,
            photo: data.photo
        });
        newPlace.save((err) => {
            if (err) {
                log("Creating new place: error: " + err);
                reject();
            } else {
                log("Creating new place: ok");
                resolve();
            }
        })
    });
}


var getPlace = function (uid) {
    return new Promise(function (resolve, reject) {
        mongo.placeM.findOne({
            uid: uid
        }).exec((err, user) => {
            if (err) return reject();
            else return resolve(user);
        });
    });
}

var getPlaces = function (a) {
    if (!a) a = {};
    return new Promise(function (resolve, reject) {
        mongo.placeM.find(a).exec((err, user) => {
            if (err) return reject();
            else return resolve(user);
        });
    });
}

var getNearPlaces = function (loc) {
    return new Promise(function (resolve, reject) {
        getPlacesInGeoRange().then((places) => {
            if (err) return reject();
            var j = (p) => {
                return u.gpsDist(loc, p.loc)
            };
            places = places.sort((a, b) => {
                return (j(a) < j(b)) ? -1 : 1
            });
            resolve(places);
        }).catch((err) => {
            reject(err)
        });
    });
}

var getPlacesInGeoRange = function (userloc) {
    return new Promise(function (resolve, reject) {
        mongo.placeM.find({}).exec((err, x) => {
            if (err) return reject();
            var j = (p) => {
                return u.gpsDist(userloc, p.loc)
            };
            x = x.filter((p) => {
                return j(p) < 50
            });
            return resolve(x);
        });
    });
}


var updatePlace = function (q, r) {
    return new Promise(function (resolve, reject) {
        mongo.placeM.findOneAndUpdate(q, {
            $set: r
        }, {}, (err, data) => {
            if (err) return reject();
            else return resolve();
        })
    });
}

var addTotcap = function (uid) {
    return new Promise(function (resolve, reject) {
        getPlace(uid).then((res) => {
            return updatePlace({
                uid: uid
            }, {
                totcap: res.totcap + 1
            });
        }).then((res) => {
            resolve();
        }).catch((err) => {
            reject(err);
        });
    });
}

var like = function (uid) {
    return new Promise(function (resolve, reject) {
        getPlace(uid).then((res) => {
            return updatePlace({
                uid: uid
            }, {
                likes: res.likes + 1
            });
        }).then((res) => {
            resolve();
        }).catch((err) => {
            reject(err);
        });
    });
}


var newAchiev = function (data) {
    log("Creating new achiev: " + data.uid);
    return new Promise(function (resolve, reject) {
        var newPlace = new mongo.achM({
            uid: data.uid,
            name: data.name,
            desc: data.desc,
            photo: data.photo,
            htc: data.htc
        });
        newPlace.save((err) => {
            if (err) {
                log("Creating new achiev: error: " + err);
                reject();
            } else {
                log("Creating new achiev: ok");
                resolve();
            }
        })
    });
}

var getAchiev = function (uid) {
    return new Promise(function (resolve, reject) {
        mongo.achM.findOne({
            uid: uid
        }).exec((err, user) => {
            if (err) return reject();
            else return resolve(user);
        });
    });
}

var getAchievs = function (a) {
    if (!a) a = {};
    return new Promise(function (resolve, reject) {
        mongo.achM.find(a).exec((err, user) => {
            if (err) return reject();
            else return resolve(user);
        });
    });
}


var getTop = function (l) {
    return new Promise(function (resolve, reject) {
        mongo.userM.find({}).sort("-score").limit(Number.parseInt(l)).exec((err, rating) => {
            if (err) return reject();
            else return resolve(rating);
        });
    });
}

var getTopInCity = function (l, c) {
    return new Promise(function (resolve, reject) {
        mongo.userM.find({
            cityid: c
        }).sort("-score").limit(Number.parseInt(l)).exec((err, rating) => {
            if (err) return reject();
            else return resolve(rating);
        });
    });
}

var _getTopInFriends = function (f) {
    return new Promise(function (resolve, reject) {
        mongo.userM.find({}).where("uid").in(f).sort("-score").exec((err, rating) => {
            if (err) return reject();
            else return resolve(rating);
        });
    });
}
var getTopInFriends = function (uid) {
    return new Promise(function (resolve, reject) {
        getUser(uid).then((user) => {
            return _getTopInFriends(user.friends);
        }).then((res) => {
            resolve(res);
        }).catch((err) => {
            reject();
        })
    });
}


var appendUserCap = function (uid, e) {
    return new Promise(function (resolve, reject) {
        mongo.userM.findOne({
            uid: uid
        }, (err, data) => {
            var n = data.cap.concat(e);
            updateUser({
                uid: uid
            }, {
                cap: n
            }).then(() => {
                resolve();
            }).catch((err) => {
                reject();
            });
        });
    });
}

var appendUserAch = function (uid, e) {
    return new Promise(function (resolve, reject) {
        mongo.userM.findOne({
            uid: uid
        }, (err, data) => {
            var n = data.ach.concat(e);
            updateUser({ uid: uid }, { ach: n }, (err) => {
                if (err) return reject();
                else return resolve();
            });
        });
    });
}

var addUserFriend = function (uid, fuid) {
    return new Promise(function (resolve, reject) {
        mongo.userM.findOne({
            uid: uid
        }, (err, data) => {
            var n = data.friends.concat(fuid);
            updateUser({
                uid: uid
            }, {
                ach: n
            }, (err) => {
                if (err) return reject();
                else return resolve();
            });
        });
    });
}


var addComment = function (data) {
    log("Creating new comment: " + data.uid);
    return new Promise(function (resolve, reject) {
        var newC = new mongo.commentM({
            uid: data.uid,
            text: data.text,
            puid: data.puid,
            user: data.user
        });
        newC.save((err) => {
            if (err) {
                log("Creating new comment: error: " + err);
                reject();
            } else {
                log("Creating new comment: ok");
                resolve();
            }
        })
    });
}

var getComments = function (uid) {
    return new Promise(function (resolve, reject) {
        mongo.commentM.find({
            puid: uid
        }).exec((err, res) => {
            if (err) return reject();
            else return resolve(res);
        });
    });
}

var getCommentsForUser = function (uid) {
    return new Promise(function (resolve, reject) {
        mongo.commentM.find({
            user: uid
        }).exec((err, res) => {
            if (err) return reject();
            else return resolve(res);
        });
    });
}


var addFriend = function (uid, newUID) {
    return new Promise(function (resolve, reject) {
        getUser(uid).then((res) => {
            return updateUser({
                uid: uid
            }, {
                friends: res.friends.concat(newUID)
            })
        }).then((res) => {
            resolve();
        }).catch((err) => {
            reject();
        });
    });
}

var addAppUid = function (uid, auid) {
    return new Promise(function (resolve, reject) {
        var newUID = new mongo.utM({
            uid: uid.uid,
            appuid: auid
        });
        newUID.save((err) => {
            if (err) {
                reject();
            } else {
                resolve(uid);
            }
        })
    });
}

var getAppUid = function (uid) {
    return new Promise(function (resolve, reject) {
        mongo.utM.findOne({
            uid: uid
        }).exec((err, res) => {
            if (err) return reject();
            else return resolve(res.appuid);
        });
    });
}

var getCntPlaces = function () {
    return new Promise(function (resolve, reject) {
        mongo.placeM.count({}).exec((err, res) => {
            if (err) return reject();
            else return resolve(res);
        });
    });
}

mongo.db.on('error', function (err) {
    console.error('connection error:', err.message);
});
mongo.db.once('open', function callback() {
    console.info("Connected to DB!");
    /*
    newPlace({
        uid: "0",
        name: "ОЦ 'Сириус'",
        desc: "Образовательный центр «Сириус» в городе Сочи создан Образовательным Фондом «Талант и успех» на базе олимпийской инфраструктуры по инициативе Президента Российской Федерации В.В. Путина. Фонд учрежден 24 декабря 2014 г. выдающимися российскими деятелями науки, спорта и искусства. Свою деятельность центр ведет на основании устава Фонда и лицензии на осуществление образовательной деятельности при поддержке и координации Министерства образования и науки Российской Федерации, Министерства спорта Российской Федерации и Министерства культуры Российской Федерации. Цель работы Образовательного центра «Сириус» – раннее выявление, развитие и дальнейшая профессиональная поддержка одарённых детей, проявивших выдающиеся способности в области искусств, спорта, естественнонаучных дисциплин, а также добившихся успеха в техническом творчестве.",
        url: "1",
        rew: 200,
        totcap: 0,
        type: 1,
        loc: [43.400940, 39.964877],
        photo: "http://www.primorsky.ru/upload/medialibrary/e54/e546652052b250a5277d0da3f41f255c.jpg"
    });

    newPlace({
        uid: "1",
        name: "Хештэг Сочи Сириус",
        desc: "#СочиСириус. Светящийся хэштег СочиСириус, установленный в парке образовательного центра Сириус",
        url: "1",
        rew: 75,
        totcap: 0,
        type: 1,
        loc: [43.400276, 39.964163],
        photo: "http://www.spbstu.ru/upload/medialibrary/a56/158.jpg"
    });

    newPlace({
        uid: "2",
        name: "Начный клуб ОЦ 'Сириус'",
        desc: "Начный клуб образовательного центра Сириус, в котором проводятся массовые мероприятия, например дискотеки, лекции, встречи с известными личностями",
        url: "1",
        rew: 60,
        totcap: 0,
        type: 1,
        loc: [43.398557, 39.964430],
        photo: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQuQzrGZk5oP3j7u4TR77toikttVvkvrZVtjbYCBvMrH7g0XcQWSA"
    });

    newPlace({
        uid: "3",
        name: "Главный медиацентр / Парк науки и искусства ОЦ Сириус",
        desc: "Главный медиацентр – это основная олимпийская информационная площадка, где в 2014 году в период проведения XXII Олимпийских зимних игр и XI. А теперь в нем занимаются ученики образовательного центра Сириус",
        url: "1",
        rew: 120,
        totcap: 0,
        type: 1,
        loc: [43.414973, 39.950321],
        photo: "http://www.center-omega.ru/upload/iblock/6dd/6ddb0e1237d387b83fd89faf132233f2.jpeg"      
    });
    
    newUser({uid: "3",
        name: "name3",
        email: "email3",
        cityid: 0,
        photo: "1"});
    newAchiev({uid: "3",
        name: "ach3",
        desc: "achdesc3",
        photo: "1",
        htc: "1"});
    */

    like("0")
});


module.exports.getPlacesInGeoRange = getPlacesInGeoRange; //+
module.exports.updatePlace = updatePlace; //+
module.exports.getNearPlaces = getNearPlaces; //+
module.exports.getPlaces = getPlaces; //+
module.exports.getPlace = getPlace; //+
module.exports.newPlace = newPlace; //+
module.exports.addTotcap = addTotcap; //+
module.exports.like = like; //+
module.exports.getCntPlaces = getCntPlaces; //+

module.exports.updateUser = updateUser; //+
module.exports.newUser = newUser; //+
module.exports.getUsers = getUsers; //+
module.exports.getUser = getUser; //+
module.exports.addFriend = addFriend; //+
module.exports.isRegistred = isRegistred; //+

module.exports.getAchievs = getAchievs; //+
module.exports.getAchiev = getAchiev; //+
module.exports.newAchiev = newAchiev; //+

module.exports.getTop = getTop; //+
module.exports.getTopInCity = getTopInCity; //+
module.exports.getTopInFriends = getTopInFriends; //+
module.exports.getTopInFriendsByArr = _getTopInFriends; //+

module.exports.appendUserCap = appendUserCap; //+
module.exports.appendUserAch = appendUserAch; //+

module.exports.getCommentsForUser = getCommentsForUser;
module.exports.getComments = getComments; //+
module.exports.addComment = addComment; //+

module.exports.addAppUid = addAppUid; //+                       
module.exports.getAppUid = getAppUid; //+                       

Array.prototype.equals = function (array) {
    if (!array || this.length != array.length)
        return false;
    for (var i = 0, l = this.length; i < l; i++) {
        if (this[i] instanceof Array && array[i] instanceof Array) {
            if (!this[i].equals(array[i]))
                return false;
        } else if (this[i] != array[i]) {
            return false;
        }
    }
    return true;
}
Object.defineProperty(Array.prototype, "equals", {
    enumerable: false
});

Array.max = function (array) {
    return Math.max.apply(Math, array);
};

Array.min = function (array) {
    return Math.min.apply(Math, array);
};