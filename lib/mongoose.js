var mongoose = require('mongoose'),
    db = mongoose.connection,
    fs = require('fs');

var url = "mongodb://dbadmin:" + fs.readFileSync('encryption/dbpass.dat').toString() + "@cluster0-shard-00-00-jhwlo.mongodb.net:27017,cluster0-shard-00-01-jhwlo.mongodb.net:27017,cluster0-shard-00-02-jhwlo.mongodb.net:27017/touristsbook?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin"

mongoose.connect(url);

var Schema = mongoose.Schema;

var User = new Schema({
    uid: { type: String, required: true },
    name: { type: String, required: true },
    email: { type: String, required: true },
    photo: { type: String, required: true, default: "https://www.shareicon.net/download/2017/01/06/868320_people_512x512.png" },
    cityid: { type: Number, required: true, default: 0 },
    score: { type: Number, required: true, default: 0 },
    tcore: { type: Number, required: true, default: 0 },
    team: { type: Number, required: true, default: 0 },
    friends: [String],
    cap: [String],
    ach: [String]
});

var Place = new Schema({
    uid: { type: String, required: true },
    name: { type: String, required: true },
    desc: { type: String, required: true },
    url: { type: String, required: true },
    rew: { type: Number, required: true, default: 0 },
    totcap: { type: Number, required: true, default: 0 },
    type: { type: Number, required: true, default: 0 },
    likes: { type: Number, required: true, default: 0 },
    loc: [Number, Number], //Double
    photo: [String]
});

var Achiev = new Schema({
    uid: { type: String, required: true },
    name: { type: String, required: true },
    desc: { type: String, required: true },
    photo: { type: String, required: true },
    htc: { type: String, required: true }
});

var ShortUser = new Schema({
    uid: { type: String, required: true },
    name: { type: String, required: true },
    cityid: { type: Number, required: true, default: 0 },
    score: { type: Number, required: true, default: 0 }
});

var Comment = new Schema({
    uid: { type: String, required: true },
    puid: { type: String, required: true },
    user: { type: String, required: true },
    text: { type: String, required: true },
    like: { type: Number, required: true, default: 0 }
});

var UserToken = new Schema({
    uid: { type: String, required: true },
    appuid: { type: String, required: true }
});

var placeM = mongoose.model('Place', Place);
var userM = mongoose.model('User', User);
var achM = mongoose.model('Achiev', Achiev);
var sUserM = mongoose.model('ShortUser', ShortUser);
var commentM = mongoose.model('Comment', Comment);
var utM = mongoose.model('UserToken', UserToken);

module.exports.placeM = placeM;
module.exports.userM = userM;
module.exports.achM = achM;
module.exports.sUserM = sUserM;
module.exports.commentM = commentM;
module.exports.utM = utM;
module.exports.db = db