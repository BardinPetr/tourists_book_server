var db = require(__dirname + "/db.js"),
    u = require(__dirname + "/utils.js"),
    admin = require("firebase-admin"),
    log = console.log;

var serviceAccount = require(__dirname + "/serviceAccountKey.json");
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://cccp-9e1f6.firebaseio.com"
});


function verifyToken(token) {
    return new Promise(function(resolve, reject) {
        admin.auth().verifyIdToken(token)
            .then(function(decodedToken) {
                resolve(decodedToken);
            }).catch(function(error) {
                log(error);
                reject();
            });
    });
}

function listAllUsers(nextPageToken) {
    return new Promise(function(resolve, reject) {
        admin.auth().listUsers(1000, nextPageToken)
            .then(function(listUsersResult) {
                listUsersResult.users.forEach(function(userRecord) {
                    console.log("user", userRecord.toJSON());
                });
                if (listUsersResult.pageToken) {
                    listAllUsers(listUsersResult.pageToken)
                }
            })
            .catch(function(error) {
                console.log("Error listing users:", error);
            });
    });
}

function sendData(topic, payload) {
    return new Promise(function(resolve, reject) {
        admin.messaging().sendToTopic(topic, payload)
            .then(function(response) {
                console.log("Successfully sent message:", response);
                resolve();
            })
            .catch(function(error) {
                console.log("Error sending message:", error);
                reject();
            });
    });
}


function sendToUser(user, type, payload) {
    return new Promise(function(resolve, reject) {
        sendData(type, {data: u.merge({user: user}, payload)}).then((res) => {
            resolve();
        }).catch((err) => {
            reject();
        });
    });
}

//sendToUser("1", "achiev", { "type": "0", "cmd": "0", "uid": "0", "data": "testtest" })

module.exports.verifyToken = verifyToken;
module.exports.listAllUsers = listAllUsers;
module.exports.sendData = sendData;

module.exports.t = {
    "update": "cccp-backend-updatenow"
};