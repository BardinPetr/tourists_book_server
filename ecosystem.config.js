module.exports = {
  /**
   * Application configuration section
   * http://pm2.keymetrics.io/docs/usage/application-declaration/
   */
  apps : [
    {
      name      : 'CCCP_API',
      script    : 'index.js',
      env: {
        COMMON_VARIABLE: 'true'
      },
      env_production : {
        NODE_ENV: 'production'
      }
    }
  ],

  /**
   * Deployment section WsxUjmPlmRdz
   * http://pm2.keymetrics.io/docs/usage/deployment/
   */
  deploy : {
    production : {
      user : 'nodedeploy',
      host : '37.46.128.174',
      ref  : 'origin/master',
      repo : 'git+ssh://git@bitbucket.org/BardinPetr/tourists_book_server.git',
      path : '/var/www/nodedeploy/cccp_prod',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    },
    dev : {
      user : 'root',
      host : '37.46.128.174',
      ref  : 'origin/develop',
      repo : 'git+ssh://git@bitbucket.org/BardinPetr/tourists_book_server.git',
      path : '/var/www/nodedeploy/cccp_dev',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env dev',
      env  : {
        NODE_ENV: 'dev'
      }
    }
  }
};
