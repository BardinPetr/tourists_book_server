'use strict';

var u = require(__dirname + "/lib/utils.js"),
    fb = require(__dirname + "/lib/firebase.js"),
    ac = require(__dirname + "/lib/achiev.js"),
    db = require(__dirname + "/lib/db.js"),
    Curl = require('node-libcurl').Curl,
    curl = new Curl(),
    fs = require("fs"),
    log = console.log;

/*
 * app - app id
 */
exports.newuser = function (req, resp) {
    log("Registration started")
    var data = req.query;
    global.f = undefined;

    fb.verifyToken(req.headers.auth_token).then((res) => {
        global.f = res;
        return db.isRegistred(res.uid);
    }).then(x => {
        if (!x) {
            log("user not registred")
            db.addAppUid(global.f, data.app).then(a => {
                return db.newUser(global.f, data);
            }).then(a => {
                log("OK")
                resp.json({
                    "res": 0
                });
                ac.sendAchFR(global.f.uid);
            }).catch(err => {
                resp.status(500);
                resp.send('we all will die\n' + err);
            });
        } else {
            log("user already registred")
            resp.json({
                "res": 1
            });
        }
    }).catch(err => {
        resp.status(500);
        resp.send('we all will die\n' + err);
        global.f = undefined;
    });
};

/*
 * return List of User
 */
exports.getusers = function (req, res) {
    var data = req.query;

    fb.verifyToken(req.headers.auth_token).then((res) => {
        return db.getUsers();
    }).then(x => {
        res.json(unique(x));
    }).catch(err => {
        res.status(500);
        res.send('we all will die: ' + err);
    });
}

/*
 * tuid - target user uid
 * 
 * return User
 */
exports.getuser = function (req, res) {
    var data = req.query;
    log("Sending user data")

    fb.verifyToken(req.headers.auth_token).then((res) => {
        return db.getUser(data.tuid);
    }).then(x => {
        res.json(x);
    }).catch(err => {
        res.status(500);
        res.send('we all will die: ' + err);
    });
};

/*
 * type - type of rating filters 0-2
 * tnum - (type = 0) top tnum users
 * cityid - (type = 1) top users in city cityid
 * uid - (type = 2) top users from friends of uid user
 * 
 * return List of Users
 */
exports.getrating = function (req, res) {
    var data = req.query;
    log("Sending rating")

    fb.verifyToken(req.headers.auth_token).then((res) => {
        data.type = Number.parseInt(data.type);
        if (data.type == 0)
            return db.getTop(data.tnum);
        else if (data.type == 1)
            return db.getTopInCity(1000, Number.parseInt(data.cityid));
        else
            return db.getTopInFriends(data.uid);
    }).then(x => {
        res.json(unique(x));
    }).catch(err => {
        res.status(500);
        res.send('we all will die: ' + err);
    });
};

/*
 * uid - target user uid
 * 
 * returns List of Users
 */
exports.getfriends = function (req, resp) {
    var data = req.query;
    log("Sending friends")

    fb.verifyToken(req.headers.auth_token).then((res) => {
        return db.getTopInFriends(data.uid);
    }).then(x => {
        resp.json(unique(x));
    }).catch(err => {
        resp.status(500);
        resp.send('we all will die: ' + err);
    });
};

/*
 * uid - user
 * tuid - friend to add to friends with user uid
 */
exports.addfriends = function (req, res) {
    var data = req.query;
    log("Adding friends")

    fb.verifyToken(req.headers.auth_token).then((res) => {
        return ac.sendAchFF(data.uid);
    }).then(x => {
        return db.addFriend(data.uid, data.tuid);
    }).then(x => {
        res.json({
            "res": 0
        });
    }).catch(err => {
        res.status(500);
        res.send('we all will die: ' + err);
    });
};

/*
 * loc - geoposition [54.958270, 54.403757] (Array of Double)
 * return List of Places
 */
exports.getplaces = function (req, res) {
    var data = req.query;
    data.loc = data.loc.map(e => {
        return parseFloat(e);
    });
    log("Sending list of places")
    fb.verifyToken(req.headers.auth_token).then((res) => {
        return db.getPlacesInGeoRange(data.loc);
    }).then(x => {
        res.json(unique(x));
    }).catch(err => {
        res.status(500);
        res.send('we all will die: ' + err);
    });
};

/*
 * loc - geoposition [54.958270, 54.403757] (Array of Double)
 * return List of Places
 */
exports.getnearplaces = function (req, res) {
    var data = req.query;
    data.loc = data.loc.map(e => {
        return parseFloat(e);
    });
    log("Sending list of places")
    fb.verifyToken(req.headers.auth_token).then((res) => {
        return db.getNearPlaces(data.loc);
    }).then(x => {
        res.json(unique(x));
    }).catch(err => {
        res.status(500);
        res.send('we all will die: ' + err);
    });
};

/*
 * uid - place to get info
 * return Place
 */
exports.getplace = function (req, res) {
    var data = req.query;
    log("Sending place")
    fb.verifyToken(req.headers.auth_token).then((res) => {
        return db.getPlace(data.uid);
    }).then(x => {
        res.json(x);
    }).catch(err => {
        res.status(500);
        res.send('we all will die: ' + err);
    });
};

/*
 * return List of Achievs
 */
exports.getachs = function (req, res) {
    var data = req.query;
    log("Sending achievements")

    fb.verifyToken(req.headers.auth_token).then((res) => {
        return db.getAchievs();
    }).then(x => {
        res.json(unique(x));
    }).catch(err => {
        res.status(500);
        res.send('we all will die: ' + err);
    });
};

/*
 * uid - achievement uid
 * return Achiev
 */
exports.getach = function (req, res) {
    var data = req.query;
    log("Sending achievement")

    fb.verifyToken(req.headers.auth_token).then((res) => {
        return db.getAchiev(data.uid);
    }).then(x => {
        res.json(x);
    }).catch(err => {
        res.status(500);
        res.send('we all will die: ' + err);
    });
};


/*
 * uid - place to get comments about
 * 
 * return List of Comments
 */
exports.getc = function (req, res) {
    var data = req.query;
    log("Sending comments")

    fb.verifyToken(req.headers.auth_token).then((res) => {
        return db.getComments(data.uid);
    }).then(x => {
        res.json(x);
    }).catch(err => {
        res.status(500);
        res.send('we all will die: ' + err);
    });
};

/*
 * text - comment text 
 * puid - comment place
 * user - who wrote comment
 */
exports.addc = function (req, res) {
    var data = req.query;
    log("Sending achievements")

    fb.verifyToken(req.headers.auth_token).then((res) => {
        return ac.sendAchFC(data['user']);
    }).then(x => {
        return db.addComment({
            uid: 0,
            text: data['text'],
            puid: data['puid'],
            user: data['user']
        });
    }).then(x => {
        res.json({
            "res": 0
        });
    }).catch(err => {
        res.status(500);
        res.send('we all will die: ' + err);
    });
};

exports.like = function (req, res) {
    var data = req.query;
    log("Like")

    fb.verifyToken(req.headers.auth_token).then((res) => {
        log(data.uid)
        return db.like(data.uid);
    }).then(x => {
        res.json({
            "res": 0
        });
    }).catch(err => {
        res.status(500);
        res.send('we all will die: ' + err);
    });
};

/*
 * loc - geoposition [54.958270, 54.403757] (Array of Double)
 * 
 * return Int:
 *   0 - OK
 *  -1 - No available places
 *  -2 - Already caught
 */

exports.newcapture = function (req, res) {
    log("Capturing");
    var data = req.query;
    data.loc = JSON.parse(data.loc);
    global.user = null;
    global.placeuid = null;
    global.f = false;
    fb.verifyToken(req.headers.auth_token).then((res) => {
        return db.getUser(res.uid);
    }).then(x => {
        global.user = x;
        return db.getPlaces();
    }).then(places => {
        var j = (p) => {
            return u.gpsDist(data.loc, p.loc)
        };

        places = places.filter((e) => {
            return global.user.cap.indexOf(e.uid) == -1
        });
        places = places.sort((a, b) => {
            return (j(a) < j(b)) ? -1 : 1
        });
        if (places.length == 0) {
            res.json({
                "res": -1
            });
        } else {
            var place = places[0];
            if (u.gpsDist(data.loc, place.loc) > 0.3) {
                res.json({
                    "res": -2
                });
                return false;
            } else {
                global.placeuid = place.uid;
                global.f = true
                return validatePhoto(req, place.name);
            }
        }
    }).then((d) => {
        if (global.f) {
            res.json({
                "res": (d ? 0 : -3)
            });
            finalizeCapture();
        }
    }).catch(err => {
        res.status(500);
        res.send('we all will die: ' + err);
    });
};


exports.capture = function (req, res) {
    log("Capturing");
    var data = req.query;
    data.loc = data.loc.map(e => {
        return parseFloat(e);
    });
    global.user = null;
    global.placeuid = null;
    global.f = false;
    fb.verifyToken(req.headers.auth_token).then((res) => {
        return db.getUser(res.uid);
    }).then(x => {
        global.user = x;
        return db.getPlaces();
    }).then(places => {
        var j = (p) => {
            return u.gpsDist(data.loc, p.loc)
        };

        places = places.filter((e) => {
            return global.user.cap.indexOf(e.uid) == -1
        });
        places = places.sort((a, b) => {
            return (j(a) < j(b)) ? -1 : 1
        });
        if (places.length == 0) {
            res.json({
                "res": -1
            });
        } else {
            var place = places[0];
            if (u.gpsDist(data.loc, place.loc) > 0.3) {
                res.json({
                    "res": -2
                });
                return false;
            } else {
                global.placeuid = place.uid;
                global.f = true
                res.json({
                    "res": 0
                });
                finalizeCapture();
            }
        }
    }).catch(err => {
        res.status(500);
        res.send('we all will die: ' + err);
    });
};


var finalizeCapture = function () {
    return new Promise(function (resolve, reject) {
        db.appendUserCap(global.user.uid, global.placeuid).then(x => {
            return db.addTotcap(global.placeuid);
        }).then(x => {
            return db.getPlace(global.placeuid);
        }).then(x => {
            return db.updateUser({
                uid: global.user.uid
            }, {
                score: global.user.score + x.rew
            });
        }).then(x => {
            return ac.sendAchPC(global.user.uid, global.placeuid);
        }).then(x => {
            resolve();
        }).catch(err => {
            reject(err);
        })
    });
}


var validatePhoto = function (req, place) {
    return new Promise(function (resolve, reject) {
        if (!req.files)
            return reject();

        let imgFile = req.files.sampleFile;
        let path = __dirname + '/public/' + imgFile.name;
        imgFile.mv(path, function (err) {
            if (err)
                return reject();

            var data = JSON.stringify({
                image_url: "http://bardin.petr.fvds.ru:667/photos/" + imgFile.name
                //image_url: "http://www.petmd.com/sites/default/files/petmd-cat-happy-10.jpg"
            });
            curl = new Curl();
            curl.setOpt(Curl.option.URL, "http://127.0.0.1:5885/search");
            curl.setOpt(Curl.option.POSTFIELDS, data);
            curl.setOpt(Curl.option.HTTPHEADER, ['User-Agent: node-libcurl/1.0', 'Content-Type: application/json']);
            curl.setOpt(Curl.option.VERBOSE, true);

            curl.perform();

            curl.on('end', function (statusCode, body) {
                fs.unlink(path, function (error) {
                    curl.close();

                    if (error)
                        return reject();

                    var x = JSON.stringify(body);
                    var r = /\\\\u([\d\w]{4})/gi;
                    x = x.replace(r, function (match, grp) {
                        return String.fromCharCode(parseInt(grp, 16));
                    });
                    x = unescape(x);
                    x = x.toLowerCase();
                    x = x.replace(/[^A-Za-zА-Яа-я]/gi, '')
                    place = place.toLowerCase();
                    place = place.split(/[^A-Za-zА-Яа-я]/);
                    var cnt = 0;
                    for (var i = 0; i < place.length; i++)
                        if (place[i] != '' && place[i] != ' ')
                            cnt += (x.match(new RegExp(place[i], "g")) || []).length;

                    return resolve(cnt > 0);
                });
            });

            curl.on('error', curl.close.bind(curl));
        });
    });
}


exports.dataFromPhoto = function (req, resp) {
    if (!req.files) {
        res.status(500);
        res.send('we all will die: ' + err);
    }

    let imgFile = req.files.sampleFile;
    let path = __dirname + '/public/' + imgFile.name;
    imgFile.mv(path, function (err) {
        if (err) {
            res.status(500);
            res.send('we all will die: ' + err);
        }

        var data = JSON.stringify({
            image_url: "http://bardin.petr.fvds.ru:667/photos/" + imgFile.name
            //image_url: "http://www.petmd.com/sites/default/files/petmd-cat-happy-10.jpg"
        });
        curl = new Curl();
        curl.setOpt(Curl.option.URL, "http://127.0.0.1:5885/search");
        curl.setOpt(Curl.option.POSTFIELDS, data);
        curl.setOpt(Curl.option.HTTPHEADER, ['User-Agent: node-libcurl/1.0', 'Content-Type: application/json']);
        curl.setOpt(Curl.option.VERBOSE, true);

        curl.perform();

        curl.on('end', function (statusCode, body) {
            fs.unlink(path, function (error) {
                curl.close();

                if (error) {
                    res.status(500);
                    res.send('we all will die: ' + err);
                } else {
                    res.json({
                        "res": 0,
                        "data": body
                    });
                }
            });
        });

        curl.on('error', curl.close.bind(curl));
    });
}

var savePhoto = function (req) {
    return new Promise(function (resolve, reject) {
        if (!req.files)
            return reject();

        let imgFile = req.files.sampleFile;
        let path = __dirname + '/public/' + imgFile.name;
        imgFile.mv(path, function (err) {
            if (err)
                return reject();
            else
                return resolve();
        });
    });
}

exports.upload = function (req, res) {
    savePhoto(req).then(res => {
        log("save ok");
        res.json({
            res: 0
        });
    }).catch(err => {
        res.status(500);
        res.send('we all will die: ' + err);
        log("error");
    });
}

exports.newplace = function (req, res) {
    var data = req.query;
    log("Creating place")
    data.loc = data.loc.map(e => {
        return parseFloat(e);
    });
    fb.verifyToken(req.headers.auth_token).then(res => {
        return db.getCntPlaces();
    }).then(r => {
        return db.newPlace({
            uid: pcnt + 1,
            name: data.name,
            desc: data.desc,
            url: "0",
            rew: 60,
            totcap: 0,
            type: 8,
            loc: data.loc,
            photo: [data.photo]
        });
    }).then(x => {
        res.json({
            "res": 0
        });
        ac.sendAchFC(data['uuid']);
    }).catch(err => {
        res.status(500);
        res.send('we all will die: ' + err);
    });
};

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min)) + min;
}

function unique(items) {
    var lookup = {};
    var result = [];
    for (var item, i = 0; item = items[i++];) {
        var name = item.name;
        if (!(name in lookup)) {
            lookup[name] = 1;
            result.push(item);
        }
    }
    return result;
}