var fileUpload = require('express-fileupload'),
    bodyParser = require('body-parser'),
    express = require('express'),
    http = require('http'),
    fs = require('fs'),
    log = console.log,
    querystring = require('querystring'),
    Curl = require('node-libcurl').Curl,
    app = express();


app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());
app.use('/photos', express.static(__dirname + '/public'));

http.createServer(app).listen(667, function () {
    console.log('Express server listening on port 667');
});