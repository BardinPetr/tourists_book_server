var fileUpload = require('express-fileupload'),
    u = require(__dirname + "/lib/utils.js"),
    db = require(__dirname + "/lib/db.js"),
    bodyParser = require('body-parser'),
    colors = require('colors/safe'),
    express = require('express'),
    routes = require('./routes'),
    https = require('https'),
    fs = require('fs'),
    log = console.log,
    querystring = require( 'querystring' ),
    Curl = require( 'node-libcurl' ).Curl,
    curl = new Curl(),
    app = express(),
    app2 = express();

require(__dirname + "/photosrv.js");

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(fileUpload());

routes(app);

https.createServer({
    key: fs.readFileSync('encryption/key.pem'),
    cert: fs.readFileSync('encryption/cert.pem'),
    passphrase: fs.readFileSync('encryption/pass.dat').toString()
}, app).listen(666);

log('API server started on port 666');